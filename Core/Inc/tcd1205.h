/*
 * tcd1205.h
 *
 *  Created on: 25 de jan de 2021
 *      Author: Lucas Muniz
 */

#ifndef INC_TCD1205_H_
#define INC_TCD1205_H_


#define CLOCK1_CHANNEL  TIM_CHANNEL_1
#define CLOCK2_CHANNEL  TIM_CHANNEL_2
#define RS_CHANNEL      TIM_CHANNEL_1
#define BT_CHANNEL		TIM_CHANNEL_2
#define SH_PORT         GPIOA
#define SH_PIN 			GPIO_PIN_4

#define SENSOR_SIZE     2096
#define ACTIVE_PIXELS   2048
#define SIGNAL_START_PIXEL 32

#include "main.h"

typedef struct
{
	uint8_t Sensor_Signal[SENSOR_SIZE];
	uint16_t Sensor_pixel;
}TCD1205_Handle;


uint8_t Data_Aquisition(TCD1205_Handle TCD1205);

uint8_t Transmit_Pattern(TCD1205_Handle TCD1205);

#endif /* INC_TCD1205_H_ */
